import type { Router } from 'vue-router'

declare global {
  interface Window {
    ethereum: any
  }
}

declare module 'pinia' {
  export interface PiniaCustomProperties {
    router: Router
  }
}
