/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{vue,js,ts,jsx,tsx}'],
	theme: {
		extend: {
			colors: {
				dark: 'var(--dark)',
				darker: 'var(--darker)',
				darkest: 'var(--darkest)',

				light: 'var(--light)',
				lighter: 'var(--lighter)',
				lightest: 'var(--lightest)',

				primary: 'var(--primary)',
				secondary: 'var(--secondary)',
				success: 'var(--success)',
				warning: 'var(--warning)',
				danger: 'var(--danger)',
				accent: 'var(--accent)',
				info: 'var(--info)',

				'danger-faded': 'var(--danger-faded)',
				'warning-faded': 'var(--warning-faded)',
				'success-faded': 'var(--success-faded)'
			},
			borderRadius: {
				sm: '5px',
				md: '7px',
				lg: '10px'
			},
			transitionDuration: {
				'25': '25ms',
				'50': '50ms'
			}
		}
	},
	plugins: [
		function ({ addVariant }) {
			addVariant('child', '& > *')
		}
	]
}
