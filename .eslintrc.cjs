/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
	root: true,
	'extends': [
		'plugin:vue/vue3-essential',
		'eslint:recommended',
		'@vue/eslint-config-typescript'
	],
	rules: {
		'no-multi-spaces': 'off',
		'eqeqeq': 'error',
		'dot-notation': 'error',
		'no-console': 'warn',
		'indent': ['error', 'tab'],
		'no-loss-of-precision': 'error',
		'space-before-function-paren': ['error', 'always'],
		'curly': ['error', 'multi-or-nest'],
		'comma-dangle': ['error', 'never'],
		'comma-spacing': ['error', { 'before': false, 'after': true }],
		'eol-last': ['error', 'always'],
		'no-multiple-empty-lines': ['error'],
		'no-new-symbol': 'error',
		'no-trailing-spaces': ['error'],
		'no-undef': ['warn'],
		'object-curly-spacing': ['error', 'always'],
		'object-shorthand': 'error',
		'prefer-const': 2,
		'quotes': ['error', 'single'],
		'semi': ['error', 'never'],
		'space-in-parens': ['error', 'never'],
		'strict': [2, 'never']
	},
	parserOptions: {
		ecmaVersion: 'latest'
	},
	ignorePatterns: [
		'src/assets'
	]
}
